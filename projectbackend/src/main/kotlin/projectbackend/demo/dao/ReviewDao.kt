package projectbackend.demo.dao

import projectbackend.demo.entity.Review

interface ReviewDao{
    fun getReviews():List<Review>
    fun save(review: Review): Review
    fun findById(id: Long): Review
    fun findByshopId(id: String): Review
}