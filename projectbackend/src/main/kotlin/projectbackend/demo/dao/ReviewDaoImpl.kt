package projectbackend.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectbackend.demo.entity.Review
import projectbackend.demo.repository.ReviewRepository

//@Profile("db")
@Repository
class ReviewDaoImpl:ReviewDao{
    override fun findByshopId(id: String): Review {
        return reviewRepository.findByshopId(id)
    }

    override fun findById(id: Long): Review {
        return reviewRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var reviewRepository: ReviewRepository
    override fun save(review: Review): Review {
        return reviewRepository.save(review)
    }

    override fun getReviews():List<Review>{
        return reviewRepository.findAll().filterIsInstance(Review::class.java)
    }
}