package projectbackend.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectbackend.demo.entity.User
import projectbackend.demo.repository.UsersRepository

//@Profile("db")
@Repository
class UserDaoImpl:UserDao{
//     override fun getUserById(): User {
//        return userRepository.findById()
//    }

    override fun findById(id: Long): User {
        return userRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var userRepository:UsersRepository
    override fun getUserByName(name: String): User {
        return userRepository.findByUserName(name)
    }

    override fun getUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)

    }

    override fun save(user: User):User{
        return userRepository.save(user)
    }
}