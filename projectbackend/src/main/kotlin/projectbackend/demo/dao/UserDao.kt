package projectbackend.demo.dao

import projectbackend.demo.entity.User

interface UserDao{
    fun getUsers():List<User>
    fun getUserByName(name:String):User
    fun save(user: User):User
    fun findById(id: Long): User
//    fun getUserById(): User
}

