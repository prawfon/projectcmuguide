package projectbackend.demo.entity.Dto

data class UserDto(
        var userName: String?=null,
        var email: String?=null,
        var firstName: String? = null,
        var lastName: String? = null,
        var imageProfile: String?=null,
        var authorities:List<AuthorityDto> = mutableListOf()
)