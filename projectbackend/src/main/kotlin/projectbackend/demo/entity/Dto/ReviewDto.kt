package projectbackend.demo.entity.Dto

import projectbackend.demo.entity.User

data class ReviewDto(
        var rating: Double?=null,
        var review:  String?=null,
        var shopId: String?=null,
        var users: User?=null
)