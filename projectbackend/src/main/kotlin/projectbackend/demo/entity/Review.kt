package projectbackend.demo.entity

import javax.persistence.*

@Entity
data class Review(
        var rating: Double?=null,
        var review:  String?=null,
        var shopId: String?=null,
        var isDeleted: Boolean = false

){
    @Id
    @GeneratedValue
    var id:Long?=null
    @ManyToOne
    var users: User?=null
}
///testtt