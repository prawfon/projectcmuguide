package projectbackend.demo.entity

import projectbackend.demo.security.entity.JwtUser
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne


@Entity
data class User(
         var userName: String?=null,
         var password: String?=null,
         var email: String?=null,
         var firstName: String? = null,
         var lastName: String? = null,
         var imageProfile: String?=null,
         var isDeleted:Boolean = false
){
    @Id
    @GeneratedValue
    var id:Long?=null
    @OneToOne
    var jwtUser: JwtUser? = null
}