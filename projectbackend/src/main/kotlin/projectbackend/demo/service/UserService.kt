package projectbackend.demo.service

import projectbackend.demo.entity.Dto.UserDto
import projectbackend.demo.entity.User

interface  UserService{
    fun getUsers():List<User>
    fun getUserByName(name:String):User
    fun save(user: User): User
    fun remove(id: Long): User
    fun getUserById(id: Long): User
}