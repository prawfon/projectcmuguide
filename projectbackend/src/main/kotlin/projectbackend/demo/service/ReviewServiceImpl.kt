package projectbackend.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectbackend.demo.dao.ReviewDao
import projectbackend.demo.entity.Review
import javax.transaction.Transactional

@Service
class ReviewServiceImpl:ReviewService{
    override fun findByshopId(id: String): Review {
        return reviewDao.findByshopId(id)
    }

    @Transactional
    override fun remove(id: Long): Review {
        val review = reviewDao.findById(id)
        review.isDeleted = true
        return review
    }

    override fun save(review: Review): Review {
        return reviewDao.save(review)
    }

    @Autowired
    lateinit var reviewDao: ReviewDao
    override fun getReviews(): List<Review> {
        return reviewDao.getReviews()
    }


}