package projectbackend.demo.service

import projectbackend.demo.entity.Dto.ReviewDto
import projectbackend.demo.entity.Review
import projectbackend.demo.entity.User

interface ReviewService{
    fun getReviews():List<Review>
    fun save(review: Review): Review
    fun remove(id: Long): Review
    fun findByshopId(id: String):Review
}