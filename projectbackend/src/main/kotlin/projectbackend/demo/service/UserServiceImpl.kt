package projectbackend.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import projectbackend.demo.dao.UserDao
import projectbackend.demo.entity.Dto.UserDto

import projectbackend.demo.entity.User
import projectbackend.demo.repository.UsersRepository
import projectbackend.demo.security.entity.Authority
import projectbackend.demo.security.entity.AuthorityName
import projectbackend.demo.security.entity.JwtUser
import projectbackend.demo.security.repository.AuthorityRepository
import projectbackend.demo.security.repository.UserRepository
import javax.transaction.Transactional

@Service
class UserServiceImpl : UserService {
    override fun getUserById(id: Long): User {
        return userDao.findById(id)
    }

    @Transactional
    override fun remove(id: Long): User {
        val user = userDao.findById(id)
        user.isDeleted=true
        return user
    }

    override fun save(user: User): User {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val cust1 = user
        val custJwt = JwtUser(
                username = cust1.userName,
                password = encoder.encode(cust1.password),
                email = cust1.email,
                enabled = true,
                firstname = cust1.firstName,
                lastname = cust1.lastName
        )
        usersRepository.save(cust1)
        userRepository.save(custJwt)
        cust1.jwtUser = custJwt
        custJwt.user = cust1
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)

        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        return userDao.save(user)
    }

    override fun getUserByName(name: String): User
    = userDao.getUserByName(name)

    @Autowired
    lateinit var userDao:UserDao
    @Autowired
    lateinit var usersRepository: UsersRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    override fun getUsers(): List<User> {
        return userDao.getUsers()
    }
}