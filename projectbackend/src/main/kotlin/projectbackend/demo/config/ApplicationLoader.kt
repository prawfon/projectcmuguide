package projectbackend.demo.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import projectbackend.demo.entity.Review
import projectbackend.demo.entity.User
import projectbackend.demo.repository.ReviewRepository
import projectbackend.demo.repository.UsersRepository
import projectbackend.demo.security.entity.Authority
import projectbackend.demo.security.entity.AuthorityName
import projectbackend.demo.security.entity.JwtUser
import projectbackend.demo.security.repository.AuthorityRepository
import projectbackend.demo.security.repository.UserRepository
import javax.transaction.Transactional

@Component
class ApplicationLoader:ApplicationRunner{
    @Autowired
    lateinit var usersRepository: UsersRepository
    @Autowired
    lateinit var reviewRepository: ReviewRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val cust1 = User(userName = "Nam",password = "123456", email = "Nam@gmail.com",firstName = "Prawfon",lastName = "thitimay")
        val custJwt = JwtUser(
                username = cust1.userName,
                password = encoder.encode(cust1.password),
                email = cust1.email,
                enabled = true,
                firstname = cust1.firstName,
                lastname = cust1.lastName
        )
        usersRepository.save(cust1)
        userRepository.save(custJwt)
        cust1.jwtUser = custJwt
        custJwt.user = cust1
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)

        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
    }

    @Transactional
    override fun run(args: ApplicationArguments?) {

        var user1= usersRepository.save(User("Wattana", "123456", "Hotzaymoon@gmail.com", "Wattana", "Potisarach","https://sudsapda.com/app/uploads/2018/07/g-dragon-e1530506722321.jpg"))
        var user2= usersRepository.save(User("Prawfon", "111122", "Prawfon@hotmai.com", "Prawfon", "Thitimatiyakul","https://assets.nst.com.my/images/articles/olsbplisins001a_1555470645.jpg"))
        var user3= usersRepository.save(User("Wisarut", "3345678", "Pan1124jooly@gmail.com", "Wisarut", "Chandutee","https://www.allkpop.com/upload/2019/03/content/190910/20190319-topjpg.jpg"))
        var user4= usersRepository.save(User("Surakanya", "888844", "Applepota24@gmail.com", "Surakanya", "Praisuwan","https://i.redd.it/wtmt6oxjj8321.jpg"))
        var review1=reviewRepository.save(Review(5.00,"good service","5"))
        var review2=reviewRepository.save(Review(4.00,"very nice place","4"))
        var review3=reviewRepository.save(Review(3.00,"cool","3"))
        var review4=reviewRepository.save(Review(3.00,"wow","2"))

        review1.users=(user1)
        review2.users=(user2)
        review3.users=(user3)
        review4.users=(user4)

        loadUsernameAndPassword()
    }
}

//5555