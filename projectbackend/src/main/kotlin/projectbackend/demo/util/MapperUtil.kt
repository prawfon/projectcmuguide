package projectbackend.demo.util


import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers
import projectbackend.demo.entity.Dto.AuthorityDto
import projectbackend.demo.entity.Dto.ReviewDto
import projectbackend.demo.entity.Dto.UserDto
import projectbackend.demo.entity.Review
import projectbackend.demo.entity.User
import projectbackend.demo.security.entity.Authority

@Mapper (componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    fun mapReviewDto(review: Review):ReviewDto
    fun mapReviewDto(reviews:List<Review>):List<ReviewDto>

    fun mapUserDto(user: User):UserDto
    fun mapUserDto(users:List<User>):List<UserDto>

    fun mapAuthority(authority: Authority):AuthorityDto
    fun mapAuthoity(authority:List<Authority>):List<AuthorityDto>
}