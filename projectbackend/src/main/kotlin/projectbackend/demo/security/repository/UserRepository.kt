package projectbackend.demo.security.repository

import org.springframework.data.repository.CrudRepository
import projectbackend.demo.security.entity.JwtUser

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}