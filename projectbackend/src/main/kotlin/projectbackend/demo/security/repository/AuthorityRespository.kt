package projectbackend.demo.security.repository

import org.springframework.data.repository.CrudRepository
import projectbackend.demo.security.entity.Authority
import projectbackend.demo.security.entity.AuthorityName

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}