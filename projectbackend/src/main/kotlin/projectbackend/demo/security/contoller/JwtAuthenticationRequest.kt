package projectbackend.demo.security.contoller
data class JwtAuthenticationRequest(var username: String? = null,
                                    var password: String? = null)