package projectbackend.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import projectbackend.demo.entity.User
import projectbackend.demo.service.UserService
import projectbackend.demo.util.MapperUtil

@RestController
class UserController{
    @Autowired
    lateinit var userService: UserService
    @GetMapping("/user")
    fun getAllUser():ResponseEntity<Any>{
        val users=userService.getUsers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(users))
    }

    @GetMapping("/users/{userid}")
    fun getUserById (@PathVariable("userId")id:Long):ResponseEntity<Any>{
        val user = userService.getUserById(id)
        return ResponseEntity.ok(user)
    }

    @GetMapping("/user/query")
    fun getUsers(@RequestParam("name")name:String):ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(userService.getUserByName(name)))
    }

    @PostMapping("/userinfo")
    fun addUser(@RequestBody user: User):ResponseEntity<Any>{
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapUserDto(userService.save(user))
        )
    }

    @PutMapping("/userupdate/{userId}")
    fun updateUser(@PathVariable("userId")id:Long?,
            @RequestBody user: User):ResponseEntity<Any>{
        user.id=id
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapUserDto(userService.save(user))
        )
    }
    @DeleteMapping("/user/{id}")
    fun deledteUser(@PathVariable("id")id:Long)
            :ResponseEntity<Any>{
        val user = userService.remove(id)
        val outputUser = MapperUtil.INSTANCE.mapUserDto(user)
        outputUser?.let{return  ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the product id is not found")
    }
}
