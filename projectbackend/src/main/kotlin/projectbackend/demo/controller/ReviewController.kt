package projectbackend.demo.controller


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import projectbackend.demo.entity.Review
import projectbackend.demo.service.ReviewService
import projectbackend.demo.util.MapperUtil


@RestController
class ReviewController{
    @Autowired
     lateinit var reviewService: ReviewService
    @GetMapping("/review")
    fun getAllReview():ResponseEntity<Any>{
        val reviews = reviewService.getReviews()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReviewDto(reviews))
    }

    @GetMapping("/review/shop/{id}")
    fun getReviewByShopID(@PathVariable id:String):ResponseEntity<Any>{
        val output = reviewService.findByshopId(id)
        var outputDto = MapperUtil.INSTANCE.mapReviewDto(output)
        outputDto?.let {return ResponseEntity.ok(it)}
        return  ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/reviewinfo")
    fun addReview(@RequestBody review: Review):ResponseEntity<Any>{
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapReviewDto(reviewService.save(review)))
    }
    @PutMapping("/reviewupdate/{reviewId}")
    fun updateUser(@PathVariable("reviewId")id:Long?,
                   @RequestBody review: Review):ResponseEntity<Any>{
        review.id=id
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapReviewDto(reviewService.save(review))
        )
    }
    @DeleteMapping("/review/{id}")
    fun deledteReview(@PathVariable("id")id:Long)
            :ResponseEntity<Any>{
        val review = reviewService.remove(id)
        val outputReview = MapperUtil.INSTANCE.mapReviewDto(review)
        outputReview?.let {return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the product id is not found") }
     return ResponseEntity.ok(reviewService.remove(id))
    }
}
//tryyyyyyy