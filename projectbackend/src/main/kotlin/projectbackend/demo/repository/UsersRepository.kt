package projectbackend.demo.repository

import org.springframework.data.repository.CrudRepository
import projectbackend.demo.entity.User

interface UsersRepository: CrudRepository<User, Long>{
    fun findByUserName(name:String):User
//    fun findByIsDeletedIsFalse():User
//    fun findByUserId():User
}