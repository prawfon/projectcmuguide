package projectbackend.demo.repository

import org.springframework.data.repository.CrudRepository
import projectbackend.demo.entity.Review


interface ReviewRepository:CrudRepository<Review,Long> {
    fun findByshopId(id: String): Review
}