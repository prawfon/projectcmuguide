import React from 'react'
import { connect } from 'react-redux'
import { push, connectRouter } from 'connected-react-router'
import { goBack } from 'connected-react-router'
import { view, Text } from 'react-native'
import {
    FooterContainer,
    HeaderText,
    HeaderIcon,
    Body,
    Center,
    HeaderContainer,
} from '../Components/styled-component'
import { Icon, TabBar } from '@ant-design/react-native'

class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'redTab',
        };
    }
    renderContent(pageText) {
        return (
            <View style={{ flex: 1, alignItems: 'center', background: 'white' }}>
                <Text style={{ margin: 50 }}>{pageText}</Text>
            </View>
        );
    }
    onChangeTab(tabName) {
        this.setState({
            selectedTab: tabName,
        });
    }

    render() {
        const { selectedTab = 'blueTab', goToHome,goToMap, goToShop, goToProfile} = this.props
        const Tab = selectedTab

        return (
            <TabBar
                unselectedTintColor="#949494"
                tintColor="#8700A9"
                barTintColor="##949494"
            >
                <TabBar.Item
                    title="HOME"
                    icon={<Icon name="home" />}
                    selected={Tab === 'Homepage'}
                    onPress={goToHome}
                />
                <TabBar.Item
                    title="MAP"
                    icon={<Icon name="compass" />}
                    selected={Tab === 'Mappage'}
                    onPress={goToMap}
                />

                <TabBar.Item
                    title="SHOP"
                    icon={<Icon name="shop" />}
                    selected={Tab === 'Shoppage'}
                    onPress={goToShop}
                />
                <TabBar.Item
                    title="PROFILE"
                    icon={<Icon name="user" />}
                    selected={Tab === 'Profilepage'}
                    onPress={goToProfile}
                />
            </TabBar>
        )
    }
}

export default connect(
    null,
    dispatch => ({
        goToHome: state => dispatch(push('./Homepage')),
        goToMap: state => dispatch(push('./Mappage')),
        goToShop: state => dispatch(push('./Shoppage')),
        goToProfile: state => dispatch(push('./Profilepage'))
    })
)(Footer)