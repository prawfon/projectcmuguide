import React from 'react'
import { View } from 'react-native';
import Footer from './Footer'
import { Body, Content } from '../Components/styled-component'

class Container extends React.Component {
    render() {
        const { children, title, hideGoBack, selectedTab, hideHeader } = this.props
        return (
           
                <View style={{ flex: 0, height: 50 }}>
                    <Footer selectedTab={selectedTab} />
                </View>
            
        )
    }
}
export default Container