import styled from 'styled-components'
import {Button,Icon} from '@ant-design/react-native'

export const Body = styled.ScrollView`
    flex: 1;
`

export const Center = styled.View`
    align-items: center;
    justify-content: center;
`
export const HeaderContainer = styled.View`
    padding-top: 3;
    padding-bottom: 8;
    background-color: #8700A9;
    flex-direction: row;
    align-items: center;
`
export const FooterContainer = styled.View`
    padding-bottom: 50;
`
export const HeaderIcon = styled.TouchableOpacity`
    padding-left: 8;
    padding-right: 8;
`
export const HeaderText = styled.Text`
    color: white;
    font-size: 24;
`