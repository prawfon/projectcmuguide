/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Router from './Containers/Routor';
import Login from './Containers/Login/Loginpage'


AppRegistry.registerComponent(appName, () => Router);
