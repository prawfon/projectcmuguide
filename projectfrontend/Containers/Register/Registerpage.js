import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native';
import { InputItem, Button } from '@ant-design/react-native';
import axios from 'axios';

class Register extends Component {
    state = {
        visible: false,
        username: '',
        password: '',
        email: '',
        firstname: '',
        lastname: ''
    }
    goToLogin = () => {
        this.props.history.push('/Loginpage')
    }
    goToRegister = () => {
        this.props.history.push('/Registerpage')
    }
    goToHome = () => {
        this.props.history.push('/Homepage')
    }
    // goToRegister = () => {
    //     this.props.history.push('/Register')
    //     // this.props.history.push('/Register', { user: this.state.email, password: this.state.password })
    // }

    // goToRegister = () => {
    //     axios({
    //         url: 'https://zenon.onthewifi.com/ticGo/users/register?email',
    //         method: 'post',
    //         data: {
    //             email: this.state.email,
    //             password: this.state.password,
    //             firstName: this.state.firstName,
    //             lastName: this.state.lastName,

    //         }
    //     }).then(res => {
    //         const { data } = res
    //         const { user } = data
    //         this.props.history.push('/Login')
    //     }).catch(e => {
    //         console.log("error ", e.response)
    //     })
    // }
    onClickRegister = async () => {
        console.log('ssssssssssssssss', this.state.username);
        console.log('ssssssssssssssss', this.state.password);
        console.log('ssssssssssssssss', this.state.email);
        console.log('ssssssssssssssss', this.state.firstname);
        console.log('ssssssssssssssss', this.state.lastname);
        axios({
            url: 'http://3.94.187.77:8888/userinfo',
            method: 'post',
            data: {
                userName: this.state.username,
                password: this.state.password,
                email: this.state.email,
                firstName: this.state.firstname,
                lastName: this.state.lastname,
            }
        }).then(response => {
            console.log('response', response);

        }).catch(e => {
            console.log("error ", e)
            // Alert.alert('Failed to register','This email has already been used. Please enter the correct email again.',[{text: 'close'}])
        })
    }

    render() {
        return (
            <ImageBackground source={require('./Login.png')} style={styles.background}>
                <View style={[styles.container, styles.transparent]} >
                    <View style={[styles.content]}>
                        <View style={[styles.layout1, styles.centerLayout1]}>
                            <View style={styles.center}>
                                <Image source={require('./Logo.png')} style={styles.profile} />
                                <Text style={styles.textLogo}>REGISTER</Text>
                            </View>

                        </View>
                        <View style={[styles.layout2, styles.centerLayout2]}>
                            <InputItem
                                type="username"
                                // value={this.state.email}
                                onChangeText={username => { this.setState({ username: username }); }}
                                placeholder="Username"
                            >
                            </InputItem>
                            <InputItem
                                type="email"
                                // value={this.state.email}
                                onChangeText={email => { this.setState({ email: email }); }}
                                placeholder="E-mail"
                            >
                            </InputItem>
                            <InputItem
                                type="number"
                                // value={this.state.password}
                                onChangeText={password => { this.setState({ password: password }); }}
                                placeholder="Password"
                            >
                            </InputItem>
                            <InputItem
                                type="text"
                                clear
                                // value={this.state.firstName}
                                onChangeText={firstname => { this.setState({ firstname: firstname }); }}
                                placeholder="First Name"
                            >
                            </InputItem>
                            <InputItem
                                type="text"
                                // value={this.state.lastName}
                                onChangeText={lastname => { this.setState({ lastname: lastname }); }}
                                placeholder="Last Name"
                            >
                            </InputItem>
                            <View style={styles.loginmiddle}>
                                <View style={styles.loginButton1}>
                                    <Button activeStyle={{ backgroundColor: 'white' }} onPress={this.onClickRegister}>REGISTER</Button>
                                </View>
                                <View style={styles.loginButton2}>
                                    <Button type="primary" onPress={() => this.goToLogin()}>BACK TO LOGIN</Button>
                                </View>
                            </View>

                            {/* <View style={styles.loginmiddle}>
                                <View style={styles.loginButton}>
                                    <Button activeStyle={{ backgroundColor: 'white' }} onPress={this.onClickRegister}>REGISTER</Button>
                                </View>
                                <View style={styles.loginButton}>
                                    <Button activeStyle={{ backgroundColor: 'white' }} onPress={this.onClickRegister}>BACK TO LOGIN</Button>
                                </View>  
                            </View> */}
                        </View>
                    </View>
                </View >
            </ImageBackground >
        );
    }
}

const styles = StyleSheet.create({

    background: {
        width: '100%',
        height: '100%'
    },

    container: {
        flex: 1
    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },
    layout1: {

        flex: 0.5,
        flexDirection: 'column'
    },
    layout2: {

        flex: 1,
        flexDirection: 'column'
    },
    textLogo: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15,
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginmiddle: {
        flex: 0.5,
        // flexDirection: 'row'
    },

    profile: {
        borderRadius: 10,
        width: 150,
        height: 150
    },
    loginButton1: {
        // flex: 1,
        // margin: 20,
        // borderRadius: 5,
        // width: 300,
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 300,
    },
    loginButton2: {
        // flex: 1,
        // margin: 20,
        // borderRadius: 5,
        // width: 300,
        flex: 1,
        marginTop: 20,
        marginLeft: 20,
        borderRadius: 5,
        width: 300,
    },
    centerLayout1: {
        top: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        top: 20,
        alignItems: 'center',

        // justifyContent: 'center'
    },
    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        flex: 1,
        margin: 15
    }

})

export default Register
