import React, { Component } from "react";
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, ScrollView,ActivityIndicator } from "react-native";
import { Icon, TabBar, Card,SearchBar, } from '@ant-design/react-native';
import axios from "axios";
import StarRating from 'react-native-star-rating';
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { categoryeng } from '../CategoryEn';
import _ from 'lodash'

const Restaurants = ["ร้านอาหาร (และเครื่องดื่ม)", "อาหารไทย", "อาหารเอเชีย"]
const Cafe = ["คาเฟ่และของหวาน"]
const Service = ["บริการต่างๆ"]
const Shop = ["ร้านค้า","ตลาด", "ของที่ระลึก", "เครื่องประดับ", "ศูนย์การค้า", "ของเก่า ของสะสม"]
const Fashion = ["แฟชั่นบุรุษ", "แฟชั่นสตรี"]
const HealthsandCosmetic = ["สุขภาพและความงาม"]
const Furniture = ["เฟอร์นิเจอร์และของตกแต่งบ้าน"]
const Miscellaneous = ["สินค้าเบ็ดเตล็ด และอื่นๆ", "ต้นไม้และอุปกรณ์ทำสวน"]

class Shoplist extends Component {
    state = {
        items: [],
        starCount: 4.5,
        // shoplists: [],
        offset: 0,
        limit: 20,
        Refresh: false,
        shopfilter: [],
        isLoading: false

    };
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         starCount: 3.5
    //         // selectedTab: 'yellowTab',
    //     };
    // }

    Restaurantsfilter = () => {
        // console.log('yoooooooo');
       const type=this.state.items.filter(type=>{ 
           return _.includes(Restaurants,type.category) 
       }) 
    //    console.log(Restaurants);
       this.setState({shopfilter:type})
    }

    cafefilter = () => {
        // console.log('yoooooooo');
       const type=this.state.items.filter(type=>{ 
           return _.includes(Cafe,type.category) 
       }) 
    //    console.log(cafe);
       this.setState({shopfilter:type})
    }
    shopfilter = () => {
        // console.log('yoooooooo');
       const type=this.state.items.filter(type=>{ 
           return _.includes(Shop,type.category) 
       }) 
    //    console.log(shop);
       this.setState({shopfilter:type})
    }
    Fashionfilter = () => {
        // console.log('yoooooooo');
       const type=this.state.items.filter(type=>{ 
           return _.includes(Fashion,type.category) 
       }) 
    //    console.log(Fashion);
       this.setState({shopfilter:type})
    }
    Servicefilter = () => {
        // console.log('yoooooooo');
       const type=this.state.items.filter(type=>{ 
           return _.includes(Service,type.category) 
       }) 
    //    console.log(Fashion);
       this.setState({shopfilter:type})
    }
    HealthsandCosmeticfilter = () => {
        // console.log('yoooooooo');
       const type=this.state.items.filter(type=>{ 
           return _.includes(HealthsandCosmetic,type.category) 
       }) 
    //    console.log(Fashion);
       this.setState({shopfilter:type})
    }

    Furniturefilter = () => {
        // console.log('yoooooooo');
       const type=this.state.items.filter(type=>{ 
           return _.includes(Furniture,type.category) 
       }) 
    //    console.log(Fashion);
       this.setState({shopfilter:type})
    }
    Miscellaneousfilter = () => {
        // console.log('yoooooooo');
       const type=this.state.items.filter(type=>{ 
           return _.includes(Miscellaneous,type.category) 
       }) 
    //    console.log(Fashion);
       this.setState({shopfilter:type})
    }

    trySomeThing = (value) => {
        var array = []
        console.log(this.props)
        this.state.items.filter(function (category) {
            if (this.state.items.filter = value) {
                arry.push(value)
            }

        })
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    goToHome = () => {
        this.props.history.push('/Homepage')
    }

    goToShop = () => {
        this.props.history.push('/ShopListpage')
    }
    goToShoppage = (item) => {
        // console.log(this.props)
        this.props.history.push('/Shoppage', {
            shop: item, index: 2

        })
    }
    goToMap = () => {
        this.props.history.push('/Mappage')
    }
    goToProfile = () => {
        this.props.history.push('/Profilepage')
        //     this.props.history.push('/Profile', { user: this.state.email, password: this.state.password })
    }

    updateSearch =(search)=>{
        this.setState({search});
        const type = this.state.items.filter(type=>{
            return type.lang.en.name.includes(search)
        })
        console.log(type);
        this.setState({shopfilter:type})
        
    }

    UNSAFE_componentWillMount() {
        this.getShoplist()
        
    }

    getShoplist = () => {
        axios.get('https:chiangmai.thaimarket.guide/shop?offset=0&limit=0'
            , { params: { offset: this.state.offset, limit: this.state.limit } }
        )
            .then(response => {
            //    this.setState({ shopfilter:response.data.data })
                 this.setState({ items: [...this.state.items, ...response.data.data] ,shopfilter:response.data.data})
             //  this.setState({shopfilter:response.data.data})
                console.log("Shop DATA==========", this.state.items);
                console.log("Shop DATA2==========", this.state.shopfilter);
                this.setState({ isLoading: true })
            })
            .catch(err => {
                console.log("shop err:", err);
                this.setState({ Refresh: false })
            })
    }
    loadShoplistMore = () => {
        if (this.state.offset === 100) {
            this.setState({ Refresh: false })
        } else {
            this.setState({
                offset: this.state.offset + this.state.limit,
                Refresh: true
            }, () => {
                this.getShoplist()
            })
        }

    }

    render() {

        return (
            <TabBar
                unselectedTintColor="white"
                tintColor="#990099"
                barTintColor="#990099"
            >
                <TabBar.Item
                    title="Home"
                    icon={<Icon name="home" />}
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToHome('Homepage')}
                >
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <TouchableOpacity
                                onPress={this.goToHome}
                            >
                                <Icon name="left" size="md" color="white" />
                            </TouchableOpacity>
                            <View style={styles.icon}>
                                <Text style={styles.textHead}>Recommended Shop</Text>
                            </View>
                        </View>
                        {/* <View >
                            <SearchBar
                                value={this.state.value}
                                placeholder="Search"
                                placeholderTextColor="#ABABAB"
                                onCancel={this.clear}
                                onChange={this.updateSearch}
                                cancelText="Cancle"
                            />
                        </View> */}
                  
                        <ScrollView horizontal={true} style={{ backgroundColor: '#EFE5F6' }} style={{ flex: 1 }}>
                            <View style={styles.category}>
                                <View style={styles.category2}>
                                    <TouchableOpacity style={[styles.box, styles.center, styles.button1]} onPress={()=>this.cafefilter()}>
                                        <Text style={[styles.textcategory]} >Cafe</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={[styles.box, styles.center, styles.button2]} onPress={()=>this.Restaurantsfilter()}>
                                        <Text style={[styles.textcategory]} >Restaurants</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={[styles.box, styles.center, styles.button1]} onPress={()=>this.shopfilter()}>
                                        <Text style={[styles.textcategory]} >Shops</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={[styles.box, styles.center, styles.button1]} onPress={()=>this.Fashionfilter()}>
                                        <Text style={[styles.textcategory]} >Fashion</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={[styles.box, styles.center, styles.button1]} onPress={()=>this.Servicefilter()}>
                                        <Text style={[styles.textcategory]} >Servies</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={[styles.box, styles.center, styles.button3]} onPress={()=>this.HealthsandCosmeticfilter()}>
                                        <Text style={[styles.textcategory]} >Healths and Cosmetic</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={[styles.box, styles.center, styles.button4]} onPress={()=>this.Furniturefilter()}>
                                        <Text style={[styles.textcategory]} >Furnitures and Decrations</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={[styles.box, styles.center, styles.button4]} onPress={()=>this.Miscellaneousfilter()}>
                                        <Text style={[styles.textcategory]} >Miscellaneous and Others</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                        <View >
                            <SearchBar
                                value={this.state.value}
                                placeholder="Search"
                                placeholderTextColor="#ABABAB"
                                onCancel={this.clear}
                                onChange={this.updateSearch}
                                cancelText="Cancle"
                            />
                        </View>

                        {this.state.isLoading ? (
                            <View style={{ margin: 5 }} style={{ flex: 10 }}>
                                <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
                                    <FlatList
                                        style={{ flex: 1 }}
                                        inverted data={this.state.shopfilter}
                                        renderItem={({ item, index }) =>
                                            // console.log(item,'sss')
                                            (
                                                <TouchableOpacity onPress={() => this.goToShoppage(item)}>
                                                    <Card style={{ backgroundColor: 'white', margin: 5 }}>
                                                        <Card.Header
                                                            title={item.lang.en.name}
                                                        // thumbStyle={{ width: 30, height: 30 }}
                                                        // extra="this is extra"
                                                        />
                                                        <Card.Body style={{ flexDirection: 'row' }}>
                                                            <View style={{ flexDirection: 'row', margin: 4 }}>
                                                                <Image source={{ uri: item.image }} style={{ width: 120, height: 150 }} />
                                                            </View>
                                                            <View style={{ width: 190, margin: 1 }}>
                                                                <Text style={styles.textShop}>{item.lang.en.name}</Text>
                                                                
                                                                <View style={styles.starrow}>
                                                                    <Text style={styles.textstar}> 4.5 </Text>
                                                                    <StarRating

                                                                        starSize={20}
                                                                        emptyStarColor='#ffcc00'
                                                                        fullStarColor='#ffcc00'
                                                                        disabled={false}
                                                                        maxStars={5}
                                                                        rating={this.state.starCount}

                                                                    />
                                                                    <Text style={styles.textstar}> (6) </Text>
                                                                </View>
                                                                <Text style={styles.categoryeng}>{categoryeng[item.category]}</Text>
                                                                <Text numberOfLines={4} style={styles.textShop}>{item.lang.en.description}</Text>
                                                            </View>
                                                        </Card.Body>
                                                    </Card>
                                                </TouchableOpacity>
                                            )
                                        }
                                    />
                                </ScrollView>
                            </View>
                        ) : (
                                <View  style={[styles.loading, styles.horizontal]}>
                                    <ActivityIndicator size="large" color="#800080"  />
                                </View>
                            )}
                    
                    </View>

                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="environment" />}
                    title="Live Map"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToMap('Mappage')}
                />

                <TabBar.Item
                    icon={<Icon name="shop" />}
                    title="Shop"
                    // selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToShop('Shoppage')}
                >
                    {/* <History /> */}
                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="Profile"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToProfile('Profilepage')}
                />
            </TabBar>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff9e6'
    },
    loading:{
        flex: 8,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
      },
    header: {
        backgroundColor: '#990099',
        // flex: 0.1,
        height: 50,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#990099',
        borderBottomWidth: 1,
        justifyContent: 'center',
        padding: 10,
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // justifyContent: 'flex-start',
    },
    textHead: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
    },
    categoryeng: {
        // flex: 1,
        textAlign: 'left',
        fontSize: 12,
        color: '#4d194d'
    },
    textShop: {
        // flex: 1,
        textAlign: 'left',
        fontSize: 15,
        color: 'black'
    },
    rowHeader: {
        flex: 1,
    },

    starrow: {
        width: 180,
        height: 30,
        margin: 8,
        marginTop: -3,
        padding: 6,
        marginLeft: -6,
        flexDirection: 'row',
        marginBottom: 6,
    },

    textstar: {
        fontSize: 14,
        color: '#ffcc00',
    },
    category: {
        backgroundColor: '#CBA2EC',
        flexDirection: 'row',
        backgroundColor: '#E3D2EE',
        padding: 6,
        height: 58,
    },
    category2: {
        flexDirection: 'row',
        marginTop: -15,
    },
    textcategory: {
        color: 'white',
        fontSize: 15

    },

    box: {
        flex: 1,
        margin: 1,
    },

    button1: {
        marginTop: 15,
        backgroundColor: '#800080',
        height: 36,
        width: 100,
        borderRadius: 19,

    },

    button2: {
        marginTop: 15,
        backgroundColor: '#800080',
        height: 36,
        width: 120,
        borderRadius: 19,

    },

    button3: {
        marginTop: 15,
        backgroundColor: '#800080',
        height: 36,
        width: 180,
        borderRadius: 19,

    },

    button4: {
        marginTop: 15,
        backgroundColor: '#800080',
        height: 36,
        width: 210,
        borderRadius: 19,

    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },


})

export default connect(null, { push })(Shoplist)
