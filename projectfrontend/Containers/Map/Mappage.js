import React, { Component } from "react";
import { View, StyleSheet,TouchableOpacity ,Image} from "react-native";
import { Icon, TabBar } from '@ant-design/react-native';
import MapView, { AnimatedRegion, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import axios from "axios";
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { categoryeng } from '../CategoryEn';

class MapPage extends Component {

    state = {
        shop: [],
        item: [],
        isLoading: false,
        visible: false,
        location: [],
        latitude: 18.80168541359868,
        longitude: 98.95131811020019,
    }

    UNSAFE_componentWillMount() {
        this.getMap()

    }
    getMap = () => {
        axios.get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0&fbclid=IwAR2ZQgBHpgRsWCeNlVN4QBF7NIKENrSfcz_SSJZgWbZuKjltez5KHLgVqVQ')
            .then(response => {
                console.log(response)
                this.setState({

                    shop: response.data.data,
                })
                for (let index = 0; index < response.data.data.length; index++) {
                    this.setState({
                        location: [...this.state.location, {
                            title: response.data.data[index].lang.th.name,
                            coordinates: {
                                latitude: parseFloat(response.data.data[index].location.latitude),
                                longitude: parseFloat(response.data.data[index].location.longitude)
                            }
                        }]
                    })
                }

            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getLocation = () => {
        const maps = this.state.shop
        console.log('showmap', maps);

        let locations = []
        if (maps.length !== undefined) {
            console.log('maplength', maps.length);
            for (let i = 0; i < maps.length; i++) {
                locations.push(
                    <Marker
                        key={i}
                        onPress={() => {
                            this.setState({
                                latitude: Number(maps[i].location.latitude),
                                longitude: Number(maps[i].location.longitude),
                                visible: true,
                                item: maps[i]
                            })
                        }}
                        title={maps[i].lang.en.name}
                        numberOfLines={2}
                        description={categoryeng[maps[i].category]}
                        // description={maps[i].lang.en.description}
                        onCalloutPress={() => { this.goToShoppage(this.state.shop[i]) }}
                        image={this.mapIcon(categoryeng[maps[i].category])}
                                        // source={this.mapIcon(this.state.shoplists[index].category)}
                                        // style={{ width: 30, height: 39 }}     
                        coordinate={{
                            latitude: Number(maps[i].location.latitude),
                            longitude: Number(maps[i].location.longitude),
                        }}
                    />
                )
            }
            return locations
        }
    }
    currentLocation = () => {
        navigator.geolocation.getCurrentPosition(position => {
            this.setState({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,

            })
        }
        );
    }

    goToHome = () => {
        this.props.history.push('/Homepage')
    }

    goToShop = () => {
        this.props.history.push('/ShopListpage')
    }

    goToShoppage = (item) => {
        // console.log(this.props)

        this.props.history.push('/Shoppage', {
            shop: item, index: 2

        })
    }

    goToMap = () => {
        this.props.history.push('/Mappage')
    }
    goToProfile = () => {
        this.props.history.push('/Profilepage')
    }
    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    componentDidMount() {
        this.getShop()
    }

    getShop = () => {
        axios.get('https://chiangmai.thaimarket.guide/shop?offset=0&limit=0')
            .then(response => {
                this.setState({
                    shops: response.data.data,
                    isLoading: false,
                    item: response.data.data[0]
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }
    mapIcon = (category) => {
        if (category === 'Shop') {
            return require('../Map/shop1.png')
        } else if (category === 'Market') {
            return require('../Map/shop1.png')
        } else if (category === 'Shopping Mall') {
            return require('../Map/shop1.png')
        }else if (category === 'Souvenirs') {
            return require('../Map/shop1.png')
        }else if (category === 'Accessories') {
            return require('../Map/shop1.png')
        }else if (category === 'Antiques and Collections') {
            return require('../Map/shop1.png')
        }else if (category === 'Restaurants(and Beverage)') {
            return require('../Map/coffee.png')
        }else if (category === 'Thai Cuisine') {
            return require('../Map/food1.png')
        } else if (category === 'Asian Cuisine') {
            return require('../Map/food1.png')
        }else if (category === 'Cafe and Desserts') {
            return require('../Map/food1.png')
        } else if (category === 'Services') {
            return require('../Map/service1.png')
        }else if (category === 'Women Fashion') {
            return require('../Map/shop1.png')
        } else if (category === 'Men Fashion') {
            return require('../Map/shop1.png')
        }else if (category === 'Healths and Cosmetic') {
            return require('../Map/service1.png')
        }else if (category === 'Furnitures and Decrations') {
            return require('../Map/Miscellaneous.png')
        } else if (category === 'Miscellaneous') {
            return require('../Map/Miscellaneous.png')
        }else if (category === 'Gardening') {
            return require('../Map/Miscellaneous.png')
        }   
    }

    render() {

        return (
            <TabBar
                unselectedTintColor="white"
                tintColor="#990099"
                barTintColor="#990099"
            >
                <TabBar.Item
                    title="Home"
                    icon={<Icon name="home" />}
                    onPress={() => this.goToHome('Homepage')}
                >
                    <View style={{ flex: 1 }}>
                        <MapView
                            showsUserLocation={true}
                            showsMyLocationButton={true}
                            showsCompass={true}
                            toolbarEnabled={true}
                            zoomEnabled={true}
                            rotateEnabled={true}
                            provider={PROVIDER_GOOGLE}
                            style={styles.map}
                            region={{
                                latitude: this.state.latitude,
                                longitude: this.state.longitude,
                                latitudeDelta: 0.02,
                                longitudeDelta: 0.0121,
                            }}
                        >
                            {this.getLocation()}
                            
                        </MapView>
                        <View onPress={this.onClose}
                            style={{ flexDirection: 'column', alignItems: 'flex-end', flex: 0, height: '100%' }}>
                            <TouchableOpacity
                                onPress={() => this.currentLocation()}
                            >
                                <Image source={require('./target.png')} style={styles.imagelocationmark} />
                            </TouchableOpacity>

                            {/* <TouchableOpacity
                                onPress={() => this.currentLocation()}
                            >
                                <Image source={require('./target.png')} style={styles.imagelocationmark2} />
                            </TouchableOpacity> */}
                        </View >
                    </View>
                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="environment" />}
                    title="Live Map"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToMap('Mappage')}
                />
                <TabBar.Item
                    icon={<Icon name="shop" />}
                    title="Shop"
                    // selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToShop('ShopListpage')}
                />

                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="Profile"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToProfile('Profilepage')}
                />
            </TabBar>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    container2: {
        flex: 1,
        backgroundColor: '#EFE5F6',
    },

    header: {
        backgroundColor: '#CBA2EC',
        flex: 0.1,
        flexDirection: 'row',
        backgroundColor: '#CBA2EC',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 6,
        height: 55,
    },
    box1: {
        flex: 0.5,
        margin: 8,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#CBA2EC'
    },

    textHead: {
        // textAlign: 'center',
        fontSize: 20,
        color: '#2A0D41',
    },

    imagelocationmark: {
        width: 30,
        height: 30,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        marginTop: 550,
        marginRight: 15,
        // borderRadius: 4,
        // marginBottom: -58,
    },

    imagelocationmark2: {
        width: 30,
        height: 30,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        marginTop: 10,
        marginRight: 20,
        borderRadius: 4,
        // marginBottom: -58,
    },

    imagepro: {
        width: '100%',
        height: '100%'
      },

    modalView: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1
    },
});

export default connect(null, { push })(MapPage)






