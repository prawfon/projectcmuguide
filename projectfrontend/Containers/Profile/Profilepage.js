import React, { Component } from "react";
import { View, Text,Image, StyleSheet, TouchableOpacity,ImageBackground } from "react-native";
import { Icon, TabBar} from '@ant-design/react-native';
import { connect } from 'react-redux'

class profile extends Component {
    state = {
        user: {},
    };

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         selectedTab: 'yellowTab',
    //     };
    // }

    // goToProfile = () => {
    //     this.props.history.push('/Profile', { user: this.state.email, password: this.state.password })
    // }
    goToLogin = () => {
        this.props.history.push('/Loginpage')
    }
    goToRegister = () => {
        this.props.history.push('/Registerpage')
    }
    goToHome = () => {
        this.props.history.push('/Homepage')
    }
    goToShop = () => {
        this.props.history.push('/ShopListpage')
    }
    goToMap = () => {
        this.props.history.push('/Mappage')
    }
    goToProfile = () => {
        this.props.history.push('/Profilepage')
    }
    render() {
        const { user, userData } = this.props
        console.log(user, 'user===============')
        return (
            <TabBar
                unselectedTintColor="white"
                tintColor="#990099"
                barTintColor="#990099"
            >
                <TabBar.Item
                    title="Home"
                    icon={<Icon name="home" />}
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToHome('Homepage')}
                >
                    <View style={styles.container}>
                        <ImageBackground
                            blurRadius={2}
                            source={{ uri: 'https://assets.nst.com.my/images/articles/olsbplisins001a_1555470645.jpg' }}
                            style={{ borderColor: '#ffffff', borderWidth: 1, height: 210, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>

                        </ImageBackground>
                        <Image style={styles.avatar} source={{ uri: 'https://assets.nst.com.my/images/articles/olsbplisins001a_1555470645.jpg' }} />
                       
                        <View style={styles.body}>
                            <View style={styles.bodyContent}>
                                <Text style={styles.name}>{user.user.userName}</Text>
                                <Text style={styles.info}>E-mail:{user.user.email}</Text>
                                <Text style={styles.info}>First name:{user.user.firstName}</Text>
                                <Text style={styles.info}>Last name:{user.user.lastName}</Text>
                                <TouchableOpacity style={styles.buttonContainer1}>
                                    <Text style={{ color: 'white' }}>EDIT</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.buttonContainer2}>
                                    <Text style={{ color: 'white' }} onPress={() => this.goToLogin()}>LOGOUT</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="environment" />}
                    title="Live Map"
                //     selected={this.state.selectedTab === 'yellowTab'}
                onPress={() => this.goToMap('Mappage')}
                />

                <TabBar.Item
                    icon={<Icon name="shop" />}
                    title="Shop"
                    // selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToShop('ShopListpage')}
                >
                    {/* <History /> */}
                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="Profile"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToProfile('Profilepage')}
                />
            </TabBar>

        )
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        userLogout: () => {
            dispatch({
                type: 'CLEAR',
            })

        }
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff9e6'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 130
    },
    body: {
        marginTop: 40,
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding: 30,
    },
    name: {
        fontSize: 28,
        color: "#330033",
        fontWeight: "600"
    },
    info: {
        fontSize: 16,
        color: "#660066",
        marginTop: 10
    },
    buttonContainer1: {
        marginTop: 30,
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 130,
        borderRadius: 30,
        backgroundColor: "#800080",
    },
    buttonContainer2: {
        marginTop: 10,
        height: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 130,
        borderRadius: 30,
        backgroundColor: "#800080",
    },
})
export default connect(mapStateToProps, mapDispatchToProps)(profile)
