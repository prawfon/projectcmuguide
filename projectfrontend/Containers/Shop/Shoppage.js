import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView, ImageBackground } from "react-native";
import { Icon, TabBar, Grid, Provider, Modal, Button, Tabs } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from "axios";
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import StarRating from 'react-native-star-rating';
import {  List, ListItem, Left, Body, Right, Thumbnail } from 'native-base';

class Shop extends Component {
    state = {
        shop: [],
        shoplang: [],
        detaillang: [],
        detailproduct: [],
        starCount: 4.5,
        rating: [],
        review: [],
        products: [],
        visible: false,
        location: {}
    }

    componentDidMount() {
        const { user } = this.props
        console.log(this.props, "usertoken");
        this.ShopById();
        this.Reviewlist();
    }

    Reviewlist = () => {
        axios({
            url: 'http://3.94.187.77:8888/review',
            method: 'get',
            headers: { "Authorization": `Bearer ${this.props.user.token}` },
        }).then(res => {
            console.log(res, "review");
            this.setState({
                rating: res.data,

            })

            // alert('successed' )
        }).catch(e => {
            alert('not successed' + e.response.data)
            console.log('res data: ', e.response.data);
            console.log('res data: ', e.response);
        }
        )
    }

    ShopById = () => {
        axios.get(`https://chiangmai.thaimarket.guide/shop/${this.props.location.state.shop.id}`)
            .then(response => {
                console.log('shop: ', response);
                this.setState({
                    shop: response.data.data,
                    shoplang: response.data.data.lang.en.name,
                    detaillang: response.data.data.lang.en.description,
                    // detailproduct: response.data.data.products.lang.en.description
                })
                console.log('shopState: ', this.state.shop);
            })
            .catch((error) => {
                console.log('Error: ' + error);
            })
    }
    ShowModel = (item) => {
        this.setState({ visible: true, products: item })
        console.log(item, 'showmodel');

    }
    onClose = () => {
        this.setState({
            visible: false,
        })
    }

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         selectedTab: 'yellowTab',
    //     };
    // }
    goToRegister = () => {
        this.props.history.push('/Registerpage')
    }
    goToHome = () => {
        this.props.history.push('/Homepage')
    }
    goToShop = () => {
        this.props.history.push('/ShopListpage')
    }
    goToMap = () => {
        this.props.history.push('/Mappage')
    }
    goToProfile = () => {
        this.props.history.push('/Profilepage')
        // this.props.history.push('/Profile', { user: this.state.email, password: this.state.password })
    }

    render() {
        //    console.log(this.state.shop,"sss")

        const tabs = [
            { title: 'About the Shop' },
            { title: 'Location' },
            { title: 'Review' },
        ];
        return (
            <TabBar
                unselectedTintColor="white"
                tintColor="#990099"
                barTintColor="#990099"
            >
                <TabBar.Item
                    title="Home"
                    icon={<Icon name="home" />}
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToHome('Homepage')}
                >

                    <View style={[styles.container]}>
                        <View style={[styles.content]}>
                            <View style={styles.header}>
                                <TouchableOpacity
                                    onPress={this.goToShop}
                                >
                                    <Icon name="left" size="md" color="white" />
                                </TouchableOpacity>
                                <View style={styles.icon}>
                                    <Text style={styles.textHead}>{this.state.shoplang}</Text>

                                </View>
                            </View>
                            {/* 
                            <View style={{ flex: 1 }}>
                                <Tabs tabs={tabs}> */}

                            <ImageBackground
                                blurRadius={2}
                                source={{ uri: this.state.shop.image }}
                                style={{ borderColor: '#ffffff', borderWidth: 1, height: 210, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                                <Image source={{ uri: this.state.shop.image }} style={styles.imageshop} />
                            </ImageBackground>



                            <View style={{ flex: 1 }}>
                                <Tabs tabs={tabs}>
                                    <ScrollView>
                                        <View >

                                            <View style={{ flexDirection: 'row' }}>
                                                <View>
                                                    <Text style={{ color: 'black', marginLeft: 10, fontSize: 22 }}>4.5</Text>
                                                </View>
                                                <View style={{ marginLeft: 5, width: 120, justifyContent: 'center' }}>
                                                    <StarRating
                                                        starStyle={marginLeft = 50}
                                                        starSize={25}
                                                        emptyStarColor='#ffcc00'
                                                        fullStarColor='#ffcc00'
                                                        disabled={false}
                                                        maxStars={5}
                                                        rating={this.state.starCount}
                                                    />
                                                </View>
                                            </View>

                                            <View style={{ borderColor: '#bec5cb', borderWidth: 1 }}>
                                                <Text style={{ color: 'black', marginLeft: 10, fontSize: 18, fontWeight: 'bold' }}>Description :</Text>
                                                <Text style={{ color: 'black', marginLeft: 10 }}>{this.state.detaillang}</Text>
                                            </View>


                                            <Text style={{ color: 'black', marginLeft: 10, fontSize: 18, fontWeight: 'bold' }}>Products :</Text>
                                            <ScrollView vertically={true} style={{ backgroundColor: '#fff9e6' }}>
                                                <Grid
                                                    // horizontal={true}
                                                    vertically={true}
                                                    columnNum={2}
                                                    hasLine={false}
                                                    data={this.state.shop.products}
                                                    itemStyle={{ height: 180, margin: 10, }}
                                                    renderItem={(item) => (
                                                        <TouchableOpacity onPress={() => this.ShowModel(item)}>
                                                            <View style={{ borderColor: '#fff9e6', borderWidth: 1 }}>
                                                                <Image style={{ width: 150, height: 150 }} source={{ uri: item.image }} />
                                                            </View>
                                                            <View style={{ borderColor: '#fff9e6', borderWidth: 1, alignItems: 'center' }}>
                                                                <Text style={{ color: 'black' }} >{item.lang.en.name}</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                    )} />
                                            </ScrollView>
                                            <Provider>
                                                <Modal
                                                    transparent={true}
                                                    visible={this.state.visible}
                                                    animationType="slide-up"
                                                    onClose={this.onClose}>
                                                    <View style={{ width: '100%', height: 400, alignItems: 'center', justifyContent: 'center' }}>
                                                        <TouchableOpacity>
                                                            <Image style={{ width: 170, height: 200, alignItems: 'center' }} source={{ uri: this.state.products.image }} />

                                                        </TouchableOpacity>
                                                        <Text style={{ color: 'black' }}>{this.state.products.lang ? this.state.products.lang.en.name : ""}</Text>
                                                        <Text numberOfLines={10}>{this.state.products.lang ? this.state.products.lang.en.description : ""}</Text>
                                                    </View>

                                                    <Button type="warning" onPress={this.onClose}>Close</Button>
                                                </Modal>
                                            </Provider>
                                        </View>


                                    </ScrollView>
                                    <View style={{ flex: 1 }}>
                                        <MapView
                                            provider={PROVIDER_GOOGLE}
                                            style={styles.map}
                                            region={{
                                                latitude: 18.8043949,
                                                longitude: 98.952629,
                                                latitudeDelta: 0.02,
                                                longitudeDelta: 0.0121,
                                            }}>
                                            <Marker
                                                identifier='ShopLocation'
                                                coordinate={{
                                                    latitude: Number(this.props.location.state.shop.location.latitude),
                                                    longitude: Number(this.props.location.state.shop.location.longitude)
                                                }}
                                            >
                                            </Marker>
                                        </MapView>

                                    </View>
                                    <View >
                                        {/* <Text>Content of Third Tab</Text> */}
                                        <ScrollView>
                                            <View >
                                                <Text style={{ color: 'black', marginLeft: 10, fontSize: 18, fontWeight: 'bold' }}>Review:</Text>

                                                {this.state.rating.length !== 0 ? (
                                                    this.state.rating.map((item, index) => {
                                                        return (
                                                            <View >
                                                                <List>
                                                                    <ListItem avatar>
                                                                        <Left>
                                                                            <Thumbnail source={{ uri: this.state.rating[index].users.imageProfile }} />
                                                                        </Left>
                                                                        <Body>

                                                                            <Text style={{ color: 'black', marginLeft: 10 }}>{this.state.rating[index].users.firstName}</Text>
                                                                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                                                            <Text style={{ color: 'black', marginLeft: 30, fontSize: 10 }}>Quality Review</Text>
                                                                                <Text style={{ color: 'black', marginLeft: 10, fontSize: 10 }}>{this.state.rating[index].rating}.0</Text>
                                                                                <StarRating
                                                                                    starStyle={marginLeft = 50}
                                                                                    starSize={10}
                                                                                    emptyStarColor='#ffcc00'
                                                                                    fullStarColor='#ffcc00'
                                                                                    disabled={false}
                                                                                    maxStars={5}
                                                                                    rating={this.state.rating[index].rating}
                                                                                // selectedStar={(rating) => this.onStarRatingPress(rating)}
                                                                                />
                                                                            </View>
                                                                            <Text style={{ color: 'black', marginLeft: 30 }}>{this.state.rating[index].review}</Text>

                                                                        </Body>
                                                                        <Right>
                                                                            {/* <Text note>3:43 pm</Text> */}
                                                                        </Right>
                                                                    </ListItem>
                                                                </List>

                                                            </View>

                                                        )
                                                    })

                                                ) : (null)}
                                            </View>

                                        </ScrollView>
                                    </View>
                                </Tabs>
                            </View>
                        </View>
                    </View>
                </TabBar.Item>

                <TabBar.Item
                    icon={<Icon name="environment" />}
                    title="Live Map"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToMap('Mappage')}
                />

                <TabBar.Item
                    icon={<Icon name="shop" />}
                    title="Shop"
                    // selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToShop('ShopListpage')}
                >
                    {/* <History /> */}
                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="Profile"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToProfile('Profilepage')}
                />
            </TabBar>

        )

    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (user) => {
            dispatch({
                type: 'ADD_USER_TOKEN',
                user: user
            })

        }
    }
}
const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff9e6'
    },
    content: {
        flex: 1
    },
    header: {
        backgroundColor: '#990099',
        // flex: 0.1,
        height: 50,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#990099',
        borderBottomWidth: 1,
        justifyContent: 'center',
        padding: 10,
    },
    icon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // justifyContent: 'flex-start',
    },
    textHead: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageshop: {
        height: 200,
        width: 200
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Shop)
