import React, { Component } from 'react';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native';
import { Provider } from 'react-redux';
import { store, history } from '../Reducer/AppStore'
import { ConnectedRouter } from 'connected-react-router'
import Login from './Login/Loginpage';
import Register from './Register/Registerpage'
import Home from './Home/Homepage'
import ShopList from './ShopList/ShopListpage'
import Mapp from './Map/Mappage'
import Profile from './Profile/Profilepage'
import Shop from './Shop/Shoppage'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <NativeRouter >
                    <Switch>
                        <Route exaat path="/Loginpage" component={Login} />
                        <Route exaat path="/Registerpage" component={Register} />
                        <Route exaat path="/Homepage" component={Home} />
                        <Route exact path="/ShopListpage" component={ShopList}/>
                        <Route exact path="/Mappage" component={Mapp}/>
                        <Route exact path="/Profilepage" component={Profile}/>
                        <Route exact path="/Shoppage" component={Shop}/>
                        <Redirect to="/Loginpage" />
                     </Switch>
                </NativeRouter> 
            </Provider>
        );
    }
}
export default Router
