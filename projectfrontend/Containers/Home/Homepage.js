import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView, ImageBackground, Linking } from "react-native";
import { Icon, TabBar,Carousel, WhiteSpace } from '@ant-design/react-native';
class home extends Component {
    state = {
        username: '',
        password: '',
        email: '',
        firstname: '',
        lastname: '',
    };

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         selectedTab: 'yellowTab',
    //     };
    // }

    goToLogin = () => {
        this.props.history.push('/Loginpage')
    }
    goToHome = () => {
        this.props.history.push('/Homepage')
    }
    goToShop = () => {
        this.props.history.push('/ShopListpage')
    }
    goToMap = () => {
        this.props.history.push('/Mappage')
    }
    goToProfile = () => {
        this.props.history.push('/Profilepage')
    }
    render() {

        return (
            <TabBar
                unselectedTintColor="white"
                tintColor="#990099"
                barTintColor="#990099"
            >
                <TabBar.Item
                    title="Home"
                    icon={<Icon name="home" />}
                //     selected={this.state.selectedTab === 'yellowTab'}
                // onPress={() => this.getShop()}
                >
                    <View style={styles.container}>

                        <View style={{ height: '30%', width: '100%', }}>
                            <ImageBackground source={require('./CMUGUIDE1.png')} style={styles.background} >
                            </ImageBackground>
                        </View>
                        <WhiteSpace></WhiteSpace>

                        <View style={{ flexDirection: 'row', marginLeft: 18 }}>
                            <View>
                                <TouchableOpacity onPress={() => Linking.openURL('https://www.wongnai.com/')}>
                                    <Image source={require('./restaurant.png')} style={{ width: 60, height: 60, marginLeft: 5 }}></Image>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => Linking.openURL('https://www.traveloka.com/en-th/')}>
                                    <Image source={require('./travel.png')} style={{ width: 60, height: 60, marginLeft: 25 }}></Image>

                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => Linking.openURL('https://www.2foodtrippers.com/chiang-mai-food-guide/')}>
                                    <Image source={require('./goods.png')} style={{ width: 60, height: 60, marginLeft: 25 }}></Image>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/reviewchiangmai/')}>
                                    <Image source={require('./hands.png')} style={{ width: 60, height: 60, marginLeft: 25 }}></Image>
                                </TouchableOpacity>
                            </View>
                        </View>


                        <View >
                            <WhiteSpace></WhiteSpace>
                            <Carousel
                                autoplay
                                infinite
                            >
                                <TouchableOpacity onPress={() => Linking.openURL('https://livingnomads.com/2018/05/best-coffee-shops-in-chiang-mai/daddys-antique-cafe-and-restaurant-chiangmai-10/')}>
                                    <Image style={{ height: 150, width: 360 }} source={require('./cafe.png')}></Image>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Linking.openURL('https://www.fourseasons.com/chiangmai/dining/')}>
                                    <Image style={{ height: 150, width: 360 }} source={require('./afternoontea.png')}></Image>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Linking.openURL('https://thailand.tripcanvas.co/chiang-mai/restaurants-with-amazing-views/')}>
                                    <Image style={{ height: 150, width: 360 }} source={require('./diner.png')}></Image>
                                </TouchableOpacity>
                            </Carousel>
                            <WhiteSpace></WhiteSpace>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                    <WhiteSpace />
                                    <Text style={{ color: 'black', fontSize: 15, textAlign: 'left' }}> Recommend shop</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity onPress={this.goToShop}>
                                        <WhiteSpace />
                                        <Text style={{ color: '#800080', fontSize: 15, textAlign: 'right' }}>Allshop > </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        <ScrollView horizontal={true}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://www.chillpainai.com/scoop/8606/')}>
                                <Image source={require('./RockMe.jpg')} style={[styles.Image]} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Linking.openURL('http://www.ohkajhuorganic.com/menu/')}>
                            <Image source={require('./Okaju.jpg')} style={[styles.Image]} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Linking.openURL('https://travelkanuman.com/food/foodcx/')}>
                            <Image source={require('./Tong.jpg')} style={[styles.Image]} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/guufusionrotiandtea')}>
                            <Image source={require('./Guu.jpg')} style={[styles.Image]} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/Zombiecafechiangmai/')}>
                            <Image source={require('./Zombiecafe.jpg')} style={[styles.Image]} />
                            </TouchableOpacity>

                        </ScrollView>
                    </View>
                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="environment" />}
                    title="Live Map"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToMap('Mappage')}
                />

                <TabBar.Item
                    icon={<Icon name="shop" />}
                    title="Shop"
                    // selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToShop('ShopListpage')}
                >

                </TabBar.Item>
                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="Profile"
                    //     selected={this.state.selectedTab === 'yellowTab'}
                    onPress={() => this.goToProfile('Profilepage')}
                />
            </TabBar>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'white'
    },
    background: {
        width: '100%',
        height: '100%'
    },
    content: {
        flex: 1,
        flexDirection: 'column'

    },
    Image: {
        width: 200,
        height: 130,
        margin: 5
    },
})

export default home
