import React, { Component } from 'react';
import {  StyleSheet, Text, View, ImageBackground, Image,Alert } from 'react-native';
import { InputItem, Button } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios';

class Login extends Component {
    state = {   
        username:'',
        password: '', 
    }
    login = async () => {
        try{
            const res = await axios.post('http://3.94.187.77:8888/auth',{
                username: this.state.username,
                password: this.state.password

            })
            const user = res.data
            console.log('ID:0',user)
            await this.props.userLogin(user)
            this.setState({isLoading:false})
            await this.props.history.push('/Homepage')
            
        }catch(error){
            this.setState({isLoading:false})
            console.log(error);
            const err = error.response.data.errors? error.response.data.error : error
            console.log('login response',error.response.data.errors);
            let errorMessage = ''
            if (err.email)errorMessage += 'Email incorrect' + '\n'
            if(err.password)errorMessage += 'Password incorrect' + '\n'
            Alert.alert('Incorrect information',errorMessage)  
        }   
    }
    goToLogin = () => {
        this.props.history.push('/Loginpage')
    }
    goToRegister = () => {
        this.props.history.push('/Registerpage')
    }
    goToHome= () => {
        this.props.history.push('/Homepage')
    }

    render() {
        //  const { username, password } = this.state
        // const { user, addUser } = this.props
        // console.log(user)
        return (
            <ImageBackground source={require('./Login.png')} style={styles.background}>
                <View style={[styles.container, styles.transparent]} >
                    <View style={[styles.content]}>
                        <View style={[styles.layout1, styles.centerLayout1]}>
                            <Text style={styles.textStyle}>
                            </Text>
                            <View style={styles.center}>
                                <Image source={require('./Logo.png')} style={styles.profile} />
                                {/* <Text style={styles.textLogo}>CMU GUIDE</Text> */}
                            </View>
                        </View>
                        <View style={[styles.layout2, styles.centerLayout2]}>
                            <InputItem
                                style={{ color: 'black' }}
                                clear
                                value={this.state.username}
                                onChange={username => {
                                    this.setState({
                                        username,
                                    });
                                }}
                                placeholder="Username"
                            >
                                <Image source={require('./email2.png')} style={styles.usernameIcon} />
                            </InputItem>
                            <InputItem
                                style={{ color: 'black' }}
                                clear
                                type="password"
                                secureTextEntry={true}
                                value={this.state.password}
                                onChange={password => {
                                    this.setState({
                                        password,
                                    });
                                }}
                                placeholder="Password"
                            >
                                <Image source={require('./key.png')} style={styles.passwordIcon} />
                            </InputItem>
                            <View style={styles.loginmiddle}>
                                <View style={styles.loginButton}>
                                    <Button type="primary" onPress={this.login}>LOGIN</Button>
                                </View>
                                <View style={styles.loginButton}>
                                    <Button  activeStyle={{ backgroundColor: 'white' }} onPress={() => this.goToRegister()}>REGISTER</Button>
                                </View>
                            </View>
                        </View>
                    </View>
                </View >

            </ImageBackground>

        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (user) => {
            dispatch({
                type: 'ADD_USER',
                user: user
            })

        }
    }
}

const styles = StyleSheet.create({

    background: {
        width: '100%',
        height: '100%'
    },

    container: {
        flex: 1
    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },
    layout1: {

        flex: 1,
        flexDirection: 'column'
    },

    layout2: {

        flex: 0.9,
        flexDirection: 'column'
    },

    loginmiddle: {
        flex: 0.5,
        
    },
    profile: {
        // borderRadius: 10,
        width: 150,
        height: 150,
        alignItems: 'center',
        justifyContent: 'center'
    },

    usernameIcon: {
        width: 25,
        height: 25
    },

    passwordIcon: {
        width: 30,
        height: 30
    },

    loginButton: {
        // backgroundColor: '#AC3333',
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 300,
       
    },
    textLogo: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15,
    },
    textStyle: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 15
    },
    
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout1: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        alignItems: 'center',
        // justifyContent: 'center'
    },

    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        flex: 1,
        margin: 15
    },

})
export default connect(null,mapDispatchToProps)(Login)
